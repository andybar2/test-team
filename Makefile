# Run development environment
dev:
	@mkdir -p .team/development
	@team env print -s "development" > .team/development/env
	@export $$(cat .team/development/env) && go run main.go
.PHONY: dev