package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Printf("Log Level: %s\n", os.Getenv("LOG_LEVEL"))
	fmt.Printf("Stripe API Key: %s\n", os.Getenv("STRIPE_API_KEY"))
}
